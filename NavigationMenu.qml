import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.11


RowLayout {
    id: nav

    height: 200
    width: 200

    Item {
        id: pan

        height: 150
        width: 150

        Button {
            width: 50
            height: 50
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter

            text: "<"
        }

        Button {
            width: 50
            height: 50
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter

            text: ">"
        }

        Button {
            width: 50
            height: 50
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter

            text: "^"
        }

        Button {
            width: 50
            height: 50
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter

            text: "v"
        }

        Button {
            width: 50
            height: 50
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter

            text: "Reset"
        }
    }

    Slider {
        orientation: Qt.Vertical
    }
}


