#include "GraphicObject.h"

GraphicObject::GraphicObject(QObject* parent) : QObject(parent) {}

void GraphicObject::setName(QString name)
{
  m_name = name;
}

void GraphicObject::addPoint(double x, double y)
{
  m_points.push_back(std::tuple<float, float>(x, y));
}

const QString& GraphicObject::getName() const
{
  return m_name;
}

const std::vector<std::tuple<float, float>>& GraphicObject::getPoints() const
{
  return m_points;
}
