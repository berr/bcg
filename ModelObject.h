#pragma once

#include "Point.h"
#include <string>
#include <vector>

struct ModelObject
{
    int id;
    std::string name;
    std::vector<Point> points;
};

