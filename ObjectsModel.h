#pragma once

#include "ModelObject.h"
#include "GraphicObject.h"
#include "Point.h"

#include <QAbstractListModel>
#include <QByteArray>
#include <QHash>
#include <QModelIndex>
#include <QObject>

#include <vector>

class ObjectsModel : public QAbstractListModel
{
  Q_OBJECT

  Q_PROPERTY(int selectedItems READ getSelectedItems NOTIFY selectedItemsChanged)

public:
  explicit ObjectsModel();

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role) const override;
  QHash<int, QByteArray> roleNames() const override;

  Q_INVOKABLE void addObject(GraphicObject* obj);
  Q_INVOKABLE void modifyObject(int id, GraphicObject* obj);

  Q_INVOKABLE void setSingleSelection(int selection_index);
  Q_INVOKABLE void toggleSelection(int selection_index);
  Q_INVOKABLE void setSelectionUntil(int selection_index);
  Q_INVOKABLE void addSelectionUntil(int selection_index);
  Q_INVOKABLE void clearSelection(bool notify=true);

  int getSelectedItems() const;
  Q_INVOKABLE void deleteSelection();


  enum Roles {
      name = Qt::UserRole + 1,
      id,
      selected,
      n_points
  };

signals:
  void selectedItemsChanged();

public slots:

private:
  static std::vector<Point> uiToModelPoints(const std::vector<std::tuple<float, float>>& ui_points);
  void setSelectionUntil(int selection_index, bool clean);
  void notifySelectionChanged(int start, int end);
  int lastIndex() const;

  std::vector<ModelObject> m_objects;
  std::vector<bool> m_selection;


  int m_object_count = 0;
};
