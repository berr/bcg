#pragma once

#include <QObject>
#include <QString>

#include <vector>

class GraphicObject : public QObject
{
  Q_OBJECT
public:
  explicit GraphicObject(QObject *parent = nullptr);

  Q_INVOKABLE void setName(QString);
  Q_INVOKABLE void addPoint(double x, double y);

  const QString& getName() const;
  const std::vector<std::tuple<float, float>>& getPoints() const;

signals:

public slots:

private:
  QString m_name;
  std::vector<std::tuple<float, float>> m_points;
};

