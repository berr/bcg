import QtQuick 2.0
import QtQuick.Layouts 1.11


ColumnLayout {

    NavigationMenu {
        width: parent.width
        height: 200
    }

    ModelEdit {
        width: parent.width
        Layout.fillHeight: true
    }
}

