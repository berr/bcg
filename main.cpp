#include "GraphicObject.h"
#include "ObjectsModel.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char* argv[])
{
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

  QGuiApplication app(argc, argv);

  QQmlApplicationEngine engine;

  ObjectsModel model;
  engine.rootContext()->setContextProperty("objectsModel", &model);

  qmlRegisterType<GraphicObject>("cg", 1, 0, "GraphicObject");
  qRegisterMetaType<GraphicObject*>();

  const QUrl url(QStringLiteral("qrc:/main.qml"));
  QObject::connect(
      &engine, &QQmlApplicationEngine::objectCreated, &app,
      [url](QObject* obj, const QUrl& objUrl) {
        if (!obj && url == objUrl)
          QCoreApplication::exit(-1);
      },
      Qt::QueuedConnection);
  engine.load(url);


  return app.exec();
}
