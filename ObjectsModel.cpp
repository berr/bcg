#include "ObjectsModel.h"

#include "Point.h"

#include <QVector>

#include <algorithm>

ObjectsModel::ObjectsModel() : QAbstractListModel(nullptr)
{
  auto current_point = 0.f;
  for (auto i = 0; i < 5; ++i)
  {
    GraphicObject created;
    created.setName(QString::fromStdString(std::string("obj") + std::to_string(i)));

    for (auto j = 0; j < 3; ++j)
    {
      created.addPoint(current_point, current_point + 1);
      current_point += 2;
    }

    addObject(&created);
  }
}

int ObjectsModel::rowCount(const QModelIndex& parent) const
{
  // single level list should not have children
  if (parent.isValid())
    return 0;

  return m_objects.size();
}

QVariant ObjectsModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid())
    return {};

  if (role < Qt::UserRole)
    return {};

  auto n = index.row();
  switch (role)
  {
    case Roles::id:
      return m_objects[n].id;
    case Roles::name:
      return QString::fromStdString(m_objects[n].name);
    case Roles::selected:
      return m_selection[n];
    case Roles::n_points:
      return static_cast<int>(m_objects[n].points.size());
  }

  return {};
}

QHash<int, QByteArray> ObjectsModel::roleNames() const
{
  auto base = QAbstractListModel::roleNames();

  base.insert(Roles::id, "objectId");
  base.insert(Roles::name, "name");
  base.insert(Roles::selected, "selected");
  base.insert(Roles::n_points, "points");

  return base;
}

void ObjectsModel::addObject(GraphicObject* obj)
{
  auto invalid_index = QModelIndex{};
  int s = m_objects.size();
  beginInsertRows(invalid_index, s, s);

  auto id = m_object_count;
  auto points = uiToModelPoints(obj->getPoints());

  m_objects.push_back(ModelObject{id, obj->getName().toStdString(), points});
  m_selection.push_back(false);
  ++m_object_count;


  endInsertRows();
}

void ObjectsModel::modifyObject(int id, GraphicObject* obj)
{
  auto found_index = -1;
  auto i = 0;
  for (const auto& o : m_objects)
  {
    if (o.id == id)
    {
      found_index = i;
      break;
    }

    ++i;
  }

  if (found_index == -1)
    return;

  auto& model = m_objects[found_index];

  model.name = obj->getName().toStdString();
  model.points = uiToModelPoints(obj->getPoints());
  emit dataChanged(index(id), index(id));
}

void ObjectsModel::setSingleSelection(int selection_index)
{
  clearSelection(false);
  m_selection[selection_index] = true;

  notifySelectionChanged(0, lastIndex());
}

void ObjectsModel::toggleSelection(int selection_index)
{
  m_selection[selection_index] = !m_selection[selection_index];

  notifySelectionChanged(selection_index, selection_index);
}

void ObjectsModel::setSelectionUntil(int selection_index)
{
  setSelectionUntil(selection_index, true);
}

void ObjectsModel::addSelectionUntil(int selection_index)
{
  setSelectionUntil(selection_index, false);
}

void ObjectsModel::clearSelection(bool notify)
{
  for (auto i = 0; i < m_selection.size(); ++i)
  {
    m_selection[i] = false;
  }

  if (notify)
  {
    notifySelectionChanged(0, lastIndex());
  }
}

int ObjectsModel::getSelectedItems() const
{
  int sum = 0;
  for (auto i = 0; i < m_selection.size(); ++i)
  {
    if (m_selection[i])
    {
      ++sum;
    }
  }

  return sum;
}

void ObjectsModel::deleteSelection()
{
  auto invalid_index = QModelIndex{};
  for (auto i = lastIndex(); i >= 0; --i)
  {
    if (m_selection[i])
    {
      beginRemoveRows(invalid_index, i, i);
      m_objects.erase(m_objects.begin() + i);
      m_selection.erase(m_selection.begin() + i);
      endRemoveRows();
    }
  }

  notifySelectionChanged(0, lastIndex());
}

std::vector<Point> ObjectsModel::uiToModelPoints(const std::vector<std::tuple<float, float>>& ui_points)
{
  auto points = std::vector<Point>();

  for (const auto& p : ui_points)
  {
    points.push_back(Point{std::get<0>(p), std::get<1>(p)});
  }

  return points;
}

void ObjectsModel::setSelectionUntil(int selection_index, bool clean)
{
  auto last_selected = 0;
  for (int i = m_selection.size() - 1; i >= 0; --i)
  {
    if (m_selection[i])
    {
      last_selected = i;
      auto invalid_index = QModelIndex{};
      break;
    }
  }

  int start;
  int end;

  if (selection_index > last_selected)
  {
    start = last_selected;
    end = selection_index;
  }
  else
  {
    start = selection_index;
    end = last_selected;
  }

  if (clean)
  {
    clearSelection(false);
  }

  for (auto i = start; i <= end; ++i)
  {
    m_selection[i] = true;
  }

  notifySelectionChanged(0, lastIndex());
}

void ObjectsModel::notifySelectionChanged(int start, int end)
{
  auto roles = QVector<int>{};
  roles.append(Roles::selected);
  emit dataChanged(index(start), index(end), roles);
  emit selectedItemsChanged();
}

int ObjectsModel::lastIndex() const
{
  return m_objects.size() - 1;
}
