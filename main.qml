import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Layouts 1.11


Window {
    id: root

    visible: true
    width: 640
    height: 480
    title: qsTr("Graphics Test")


    RowLayout {
        anchors.fill: parent

        Configuration {
            width: 320
            Layout.fillHeight: true
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "black"
        }

    }
}
