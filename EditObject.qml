import QtQuick 2.0
import QtQuick.Controls 2.4
import cg 1.0

Dialog {
    id: dialog
    title: "Edit Object Properties"
    standardButtons: Dialog.Ok | Dialog.Cancel
    modal: true

    width: 400
    height: 400

    property bool creating: false
    property int id: -1

    contentItem: Column {
        Row {
            Text {
                text: "Name:"
            }
            TextInput {
                id: name
                text: "taeu"
            }
        }

        Row {
            Text {
                text: "p0:"
            }

            Text {
                text: "x:"
            }
            DoubleSpinBox {
                id: p0_x
            }

            Text {
                text: "y:"
            }
            DoubleSpinBox {
                id: p0_y
            }
        }

        Row {
            Text {
                text: "p1:"
            }

            Text {
                text: "x:"
            }
            DoubleSpinBox {
                id: p1_x
            }

            Text {
                text: "y:"
            }
            DoubleSpinBox {
                id: p1_y
            }
        }

        Row {
            Text {
                text: "p2:"
            }

            Text {
                text: "x:"
            }
            DoubleSpinBox {
                id: p2_x
            }

            Text {
                text: "y:"
            }
            DoubleSpinBox {
                id: p2_y
            }
        }
    }

    property GraphicObject obj: GraphicObject{}

    onAccepted: {
        obj.setName(name.text);
        obj.addPoint(p0_x.valueFromText, p0_y.valueFromText);
        obj.addPoint(p1_x.valueFromText, p1_y.valueFromText);
        obj.addPoint(p2_x.valueFromText, p2_y.valueFromText);
        if (creating)
            objectsModel.addObject(obj);
        else
            objectsModel.modifyObject(id, obj);
    }
}



