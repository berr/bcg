import QtQuick 2.11
import QtQuick.Controls 1.4

Item {
    id: root


    Rectangle {
        id: listBackground
        width: 300
        height: 200
        color: "blue"
    }


    Component {
        id: itemDelegate

        Rectangle {
            id: itemBackground
            property bool hovered: false

            width: listView.width
            height: 20
            color: selected? "white" : hovered? "black" : "red"

            Text {
                text: objectId + " - " + name + " - " + points
                color: hovered? "red": "black"
            }

            MouseArea {
                anchors.fill: itemBackground
                hoverEnabled: true

                onHoveredChanged: {
                    parent.hovered = containsMouse
                }

                onClicked: {
                    if (mouse.modifiers & Qt.ShiftModifier & Qt.ControlModifier ) {
                        objectsModel.addSelectionUntil(index);
                    } else if (mouse.modifiers & Qt.ControlModifier) {
                        objectsModel.toggleSelection(index);
                    } else if (mouse.modifiers & Qt.ShiftModifier) {
                        objectsModel.setSelectionUntil(index);
                    } else {
                        objectsModel.setSingleSelection(index);
                    }
                }

                onDoubleClicked: {
                    var dialog = Qt.createComponent("qrc:/EditObject.qml")

                    if (dialog.status !== Component.Ready) {
                        console.error("wrong status: ", dialog.errorString)
                        return;
                    }


                    var popup = dialog.createObject(root, {"creating" : false, "id" : objectId} );
                    popup.open();
                }
            }
        }
    }

    Component {
        id: highlightDelegate

        Rectangle {
            width: listView.width
            height: 20
            color: "green"
            y: listView.currentItem.y;
        }
    }


    ListView {
        id: listView

        anchors.centerIn: listBackground
        width: listBackground.width - 2
        height: listBackground.height - 2

        model: objectsModel

        delegate: itemDelegate
        highlight: highlightDelegate

        boundsBehavior: Flickable.StopAtBounds
        clip: true
        interactive: true
        focus: true
    }


    Item {
        id: listEdit

        anchors.top: listBackground.bottom
        anchors.topMargin: 2

        anchors.left: listBackground.left
        anchors.right: listBackground.right

        Text {
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            text: objectsModel.selectedItems
        }


        height: 20

        Button {
            width: 20
            height: 20

            anchors.top: parent.top
            anchors.left: parent.left

            text: "+"

            onClicked: {
                var dialog = Qt.createComponent("qrc:/EditObject.qml")

                if (dialog.status !== Component.Ready) {
                    console.error("wrong status: ", dialog.errorString)
                    return;
                }


                var popup = dialog.createObject(root, {"creating" : true});
                popup.open();
            }
        }

        Button {
            width: 20
            height: 20

            anchors.top: parent.top
            anchors.right: parent.right

            enabled: objectsModel.selectedItems > 0

            text: "-"

            onClicked: objectsModel.deleteSelection()
        }
    }
}
